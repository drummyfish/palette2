#!/bin/bash

PROGRAM=test

clear; clear; g++ -x c -g -fmax-errors=5 -pedantic -O3 -Wall -Wextra -o $PROGRAM $PROGRAM.c 2>&1 >/dev/null && ./$PROGRAM
