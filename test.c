/*
  Palette test.

  by Drummyfish, released under CC0 1.0
*/

#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include "palette.h"
#include "image.h"

void saveImage(int number, int w, int h, const uint8_t *image)
{
  char fileName[11] = "testXX.ppm";

  fileName[4] = '0' + number / 10;
  fileName[5] = '0' + number % 10;
  
  printf("saving image file: %s\n",fileName);

  FILE *f = fopen(fileName,"w");
  
  fprintf(f,"P3\n%d %d\n255\n",w,h);

  for (int i = 0; i < w * h; ++i)
  {
    int index = image[i] * 3;
    fprintf(f,"%d %d %d\n",paletteRGB8[index],paletteRGB8[index + 1],paletteRGB8[index + 2]);
  }

  fclose(f);
}

uint8_t processedImage[256 * 256];

int main()
{
  int num = 0;  

  saveImage(num,256,256,testImage);

  num++;

  for (int i = -10; i <= 10; i++)
  {
    memcpy(processedImage,testImage,256 * 256);

    for (int j = 0; j < 256 * 256; ++j)
      processedImage[j] = palette_addValue(processedImage[j],i);

    saveImage(num,256,256,processedImage);
    num++;
  }

  for (int i = 0; i <= 10; i++)
  {
    memcpy(processedImage,testImage,256 * 256);

    for (int j = 0; j < 256 * 256; ++j)
      processedImage[j] = palette_minusValue(processedImage[j],i);

    saveImage(num,256,256,processedImage);
    num++;
  }

  for (int i = 0; i <= 10; i++)
  {
    memcpy(processedImage,testImage,256 * 256);

    for (int j = 0; j < 256 * 256; ++j)
      processedImage[j] = palette_plusValue(processedImage[j],i);

    saveImage(num,256,256,processedImage);
    num++;
  }

  return 0;
}
